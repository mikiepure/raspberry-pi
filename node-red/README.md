# Node-RED for my home

Node-RED を使用した、自宅用の設定を記載する。

## 赤外線ツール設定

    cd irda
    curl http://abyz.me.uk/rpi/pigpio/code/irrp_py.zip | zcat > irrp.py

## Node-RED ノード追加

以下のノードを追加する。

- node-red-node-swagger

## Beebottle 連携

フローの読み込み後、下記 URL を参考に、MQTT の設定を行う。

- https://gitlab.com/mikiepure/raspberry-pi/-/wikis/Node-RED#beebottle
