""" Say something to Google Home. """
import hashlib
import json
import socket

import pychromecast
import requests
from flask import Blueprint, Flask, jsonify, request

from util.log import LOG
from util.path import ROOT_DIR

IP_ADDR = socket.gethostbyname("raspberrypi.local")
IP_PORT = 3333

API = Flask("Google Home Speaker")
GOOGLE_HOME = None


@API.route("/speak", methods=['POST'])
def on_speak():
    """ Speak a text on Google Home. """
    data = request.get_json()
    LOG.info("speak text: %s", data)

    data_md5 = hashlib.md5(json.dumps(data).encode("utf-8")).hexdigest()
    wav_path = ROOT_DIR / "static" / "data" / (data_md5 + ".wav")
    if not wav_path.exists():
        # API manual: https://cloud.voicetext.jp/webapi/docs/api
        LOG.debug("get voice audio from VoiceText Web API")
        voice_text_resp = requests.post(
            "https://api.voicetext.jp/v1/tts",
            data=data,
            auth=("6h47vh97zrh1dlqu", ""),
        )
        if voice_text_resp.status_code != 200:
            resp_data = voice_text_resp.json()
            resp = jsonify(resp_data)
            resp.status_code = voice_text_resp.status_code
            LOG.error("failed to speak text by error: %s", resp_data)
            return resp

        LOG.debug("store voice audio to file: %s", str(wav_path))
        with wav_path.open("wb") as wfp:
            wfp.write(voice_text_resp.content)

    if GOOGLE_HOME is not None:
        playurl = "http://{}:{}/data/{}".format(IP_ADDR, IP_PORT,
                                                wav_path.name)
        LOG.info("playing: %s", playurl)
        GOOGLE_HOME.wait()
        GOOGLE_HOME.media_controller.play_media(
            playurl,
            "audio/wav",
        )
        GOOGLE_HOME.media_controller.block_until_active()

    resp = jsonify()
    resp.status_code = 200
    return resp


def main():
    """ Entry point of the application. """

    # search chromecast devices (incl. google home)
    chromecasts = pychromecast.get_chromecasts()
    if len(chromecasts) == 0:
        LOG.fatal("failed to find google home device: no chromecast device")
        exit(1)

    # there is one chromecast device in my house
    global GOOGLE_HOME
    GOOGLE_HOME = chromecasts[0]

    uidir = ROOT_DIR / "static" / "ui"
    API.register_blueprint(Blueprint("ui", __name__, static_folder=str(uidir)))

    datadir = ROOT_DIR / "static" / "data"
    datadir.mkdir(parents=True, exist_ok=True)
    API.register_blueprint(
        Blueprint("data", __name__, static_folder=str(datadir)))

    API.run(host="0.0.0.0", port=IP_PORT)


if __name__ == "__main__":
    main()
