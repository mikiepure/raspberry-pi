""" Provide logger of the application. """
from datetime import datetime
from logging import (
    FileHandler,
    Formatter,
    Logger,
    StreamHandler,
    getLogger,
    getLogRecordFactory,
    setLogRecordFactory,
)

from util.path import LOG_DIR

###############################################################################
# Public
###############################################################################


def get_logger(name: str, to_stream: bool = True,
               to_file: bool = False) -> Logger:
    """ Get logger of the application.

    Args:
        name: Logger name.

    Returns:
        logger for the application

    """
    # setup logger
    logger = getLogger(name)
    logger.setLevel("DEBUG")
    logger.propagate = False

    # setup stream handler
    if to_stream:
        stream_handler = StreamHandler()
        stream_handler.setLevel("DEBUG")
        stream_handler.setFormatter(
            Formatter("%(tick)0.3f %(levelname5)s %(message)s",
                      "%Y/%m/%d %H:%M:%S"))
        logger.addHandler(stream_handler)

    # setup file handler
    if to_file:
        file_path = LOG_DIR / datetime.now().strftime("%Y%m%d-%H%M%S") + ".txt"
        file_handler = FileHandler(str(file_path))
        file_handler.setLevel("DEBUG")
        file_handler.setFormatter(
            Formatter(
                "%(asctime)s.%(msecs)03d %(tick)0.3f %(process)04x %(thread)04x "
                "%(levelname5)s %(message)s "
                "(%(filename)s:%(lineno)d:%(funcName)s)",
                "%Y/%m/%d %H:%M:%S",
            ))
        logger.addHandler(file_handler)

    return logger


# logger of the application
LOG = get_logger("Google Home Speaker")

###############################################################################
# Private
###############################################################################

# a table to convert levelno to original levelname
_LEVELNO_NAME5 = {
    0: "NOSET",
    10: "DEBUG",
    20: "INFO.",
    30: "WARN.",
    40: "ERROR",
    50: "FATAL",
}


def _record_factory(*args, **kwargs):
    """ Inject extended record factory. """
    record = _OLD_FACTORY(*args, **kwargs)
    record.tick = record.relativeCreated / 1000.0
    record.levelname5 = _LEVELNO_NAME5.get(record.levelno, "NOSET")
    return record


# extend log record
_OLD_FACTORY = getLogRecordFactory()
setLogRecordFactory(_record_factory)
