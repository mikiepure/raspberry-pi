""" Provide path information of the application. """
from pathlib import Path

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
LOG_DIR = ROOT_DIR / "log"
