@echo off
cd %~dp0

call ".venv\Scripts\activate.bat"
echo [DEBUG] virtual environment activated

python src\main.py
set err=%errorlevel%
if %err% neq 0 (
  echo [ERROR] exit python script by error: %err%
  pause
  exit /b 1
)
