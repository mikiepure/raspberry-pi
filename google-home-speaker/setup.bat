@echo off
cd /d %~dp0

if not exist ".venv" (
  goto CREATE_VENV
)

echo [WARN.] virtual environment is already created
set /p re_inst="Do you want to re-create it? [y/N]: "
if "%re_inst%" == "Y" (
  goto REMOVE_VENV
) else if "%re_inst%" == "y" (
  goto REMOVE_VENV
) else (
  exit /b 0
)

:REMOVE_VENV
echo [INFO.] removing virtual environment...
rmdir /s /q ".venv"
if exist ".venv" (
  echo [ERROR] failed to remove virtual environment
  pause
  exit /b 1
)

:CREATE_VENV
echo [INFO.] creating virtual environment...
py -3.5 -m venv ".venv"
set err=%errorlevel%
if %err% neq 0 (
  echo [ERROR] failed to create virtual environment by error: %err%
  pause
  exit /b 1
)

call ".venv\Scripts\activate.bat"
echo [DEBUG] virtual environment activated

echo [INFO.] upgrading pip to latest version...
python -m pip install --upgrade pip
set err=%errorlevel%
if %err% neq 0 (
  echo [ERROR] failed to upgrade pip to latest version by error: %err%
  pause
  exit /b 1
)

echo [INFO.] installing required packages...
pip install -r requirements.txt
set err=%errorlevel%
if %err% neq 0 (
  echo [ERROR] failed to install required packages by error: %err%
  pause
  exit /b 1
)
