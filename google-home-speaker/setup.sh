#!/bin/bash

if [ -d ".venv" ]; then
  echo [WARN.] virtual environment is already created
  echo -n "Do you want to re-install it? [y/N]: "
  read re_inst
  if [ "$re_inst" = "Y" ] || [ "$re_inst" = "y" ]; then
    echo [INFO.] removing virtual environment...
    rm -rf ".venv"
    if [ -d ".venv" ]; then
      echo [ERROR] failed to remove virtual environment
      exit 1
    fi
  else
    exit 0
  fi
fi

echo [INFO.] creating virtual environment...
python3 -m venv ".venv"
err=$?
if [ "$err" != "0" ]; then
  echo [ERROR] failed to create virtual environment by error: $err
  exit 1
fi

source ".venv/bin/activate"
echo [DEBUG] virtual environment activated

echo [INFO.] upgrading pip to latest version...
python -m pip install --upgrade pip
err=$?
if [ "$err" != "0" ]; then
  echo [ERROR] failed to upgrade pip to latest version by error: $err
  exit 1
fi

echo [INFO.] installing required packages...
pip install -r requirements.txt
err=$?
if [ "$err" != "0" ]; then
  echo [ERROR] failed to install required packages by error: $err
  exit 1
fi
