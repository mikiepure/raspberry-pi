#!/bin/bash

source ".venv/bin/activate"
echo [DEBUG] virtual environment activated

python src/main.py
err=$?
if [ "$err" != "0" ]; then
  echo [ERROR] exit python script by error: $err
fi
